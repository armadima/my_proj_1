<?php

$connect = mysqli_connect("127.0.0.1","mysql","mysql","proj_db") or mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
if ($connect == false){
	echo "There is no connection to db";
	echo mysqli_connect_error();
	exit();
}
$query = "SELECT * FROM users ORDER BY id";

$result = mysqli_query($connect, $query)  or trigger_error(mysqli_error()." in ". $query);



?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
		content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">

	
	<!-- Bootstrap CSS Bootstrap CSS-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!-- fontawesome, but take icons from Cheatsheet -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
	
	<link rel="stylesheet" href="\css\my_proj_1.css">
	<title>my_proj_1</title>
</head>

<body>

	<header id="header" class="header">		
		<div class="container">
			<div class="row">
				<div class="col-lg-3">
					<div class="logo">
						<img src="https://upload.wikimedia.org/wikipedia/ru/a/a2/Эрудиция_фильм_2012_постер.jpg" alt="logo">
					</div>
				</div>
				<div class="col-lg-5 ml-auto">
					<nav>
						<ul class="menu">
							<li class="menu__item">
								<a href="#">link 1</a>
								<a href="#">link 2</a>
								<a href="#">link 3</a>
							</li>
						</ul>
					</nav>
				</div>				
			</div>
			<div class="row">
				<div class="col-lg-6">

					<div class="offer">
						<h1 class="offer__title">
							The table
						</h1>

						<div class="offer__intro">
								<h3>Design is a funny word. Some people think design means how it looks. But of course, if you dig deeper, it's really how it works. Steve Jobs </h3>
						</div>
							<p class="offer__text">
								Lorem ipsum dolor sit amet, consectetur adipisicin eliy. Atque, possimus quos provident, quo culpa sapiente!
								Lorem ipsum dolor sit amet, consectetur adipisicing eli. Atque, possimus quos provident, quo culpa sapiente!
							</p>
						</div>
					
				</div>

				<div class="col-lg-5 ml-auto">
					<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQjJiViCnLx_FsO92DlH8-m8eljHsKG6ID6ld2acBqC6IW98Q8L" class="colosseus" alt="colosseus">				
				</div>				
			</div>			
		</div>
	</header>

	<section id="content" class="content">
		<div class="container">
			<div class="row">
				<div class="container-fluid">
			    	<div class = "table-responsive">
				    	<table id="proj_db" class="table table-striped table-bordered"> 
				    		<thead>
				    			<tr>
				    				<td>ID</td>
				    				<td>First name</td>
				    				<td>Last name</td>
				    				<td>Birth date</td>
				    				<td>Height</td>
				    				<td>Weight</td>
				    				<td>Pass</td>
				    				<td>Info</td>
				    				<td>Rights</td>
				    			</tr>
				    		</thead>
				    		<?php
				    		while($row= mysqli_fetch_array($result))
				    		{
				    			echo '
				    			<tr>
				    				<td>'.$row ["id"].'</td>
				    				<td>'.$row ["first_name"].'</td>
				    				<td>'.$row ["last_name"].'</td>
				    				<td>'.$row ["birth_date"].'</td>
				    				<td>'.$row ["height"].'</td>
				    				<td>'.$row ["weight"].'</td>
				    				<td>'.$row ["pass"].'</td>
				    				<td>'.$row ["info"].'</td>
				    				<td>'.$row ["rights"].'</td>
				    			</tr>    		
				    			';
				    		}

				    		?>
				    	</table>
				    </div>
	    		</div>				
			</div>			
		</div>
	</section>

	<section id="touch" class="touch">
		<div class="container">
			<div class="row"></div>			
		</div>
	</section>
	<!--
	<content id="content" class="content"></content>
	-->
	<footer id="footer" class="footer">
		<div class="container">
			<div class="row">
				
							<ul class="icons d-flex">
								<li class="icons__item">
									<a href="#">
										<i class="fab fa-apple"></i>
									</a>
								</li>
								<li class="icons__item">
									<a href="#">
										<i class="fab fa-android"></i>
									</a>
								</li>
								<li class="icons__item">
									<a href="#">
										<i class="fab fa-windows"></i>
									</a>
								</li>
							</ul>
			</div>			
		</div>
	</footer>


	<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
 
</body>
</html>